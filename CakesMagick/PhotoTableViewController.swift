//
//  PhotoTableViewController.swift
//  CakesMagick
//
//  Created by Ajmera, Prashuk (Cognizant) on 02/11/17.
//  Copyright © 2017 Cakes N Magick. All rights reserved.
//

import UIKit

var uploadImage = UIImage(named: "dummyPhoto")

class PhotoTableViewController: UITableViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var imageView: UIImageView!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2
        self.imageView.clipsToBounds = true
        self.imageView.layer.borderWidth = 3.0
        self.imageView.layer.borderColor = UIColor.gray.cgColor
        imagePicker.delegate = self
    }

    // MARK: - Table view data source    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath == [0,0]) {
            let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let deleteAction = UIAlertAction(title: "Delete Photo", style: .destructive, handler: {
                (alert: UIAlertAction!) -> Void in
                self.imageView.image = UIImage(named: "dummyPhoto")
                if let index = self.tableView.indexPathForSelectedRow{
                    self.tableView.deselectRow(at: index, animated: true)
                }
            })
            let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera;
                    self.imagePicker.allowsEditing = true
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                if let index = self.tableView.indexPathForSelectedRow{
                    self.tableView.deselectRow(at: index, animated: true)
                }
            })
            let photoAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .savedPhotosAlbum;
                    self.imagePicker.allowsEditing = true
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                if let index = self.tableView.indexPathForSelectedRow{
                    self.tableView.deselectRow(at: index, animated: true)
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                if let index = self.tableView.indexPathForSelectedRow{
                    self.tableView.deselectRow(at: index, animated: true)
                }
            })
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(cameraAction)
            optionMenu.addAction(photoAction)
            optionMenu.addAction(cancelAction)
            self.present(optionMenu, animated: true, completion: nil)
            
        }
        if (indexPath == [1,0]) {
            self.imageView.image = UIImage(named: "dummyPhoto")
            if let index = self.tableView.indexPathForSelectedRow{
                self.tableView.deselectRow(at: index, animated: true)
            }
        }
        if (indexPath == [1,1]) {
            let alert = UIAlertController(title: "Thank You", message: "Photo successfully uploaded", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                switch action.style{
                case .default:
                    uploadImage = self.imageView.image
                    if let index = self.tableView.indexPathForSelectedRow{
                        self.tableView.deselectRow(at: index, animated: true)
                    }
                case .cancel:
                    print("Cancel")
                case .destructive:
                    print("Destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as? UIImage
        imageView.image = chosenImage
        imageView.contentMode = .scaleAspectFit
        dismiss(animated:true, completion: nil)
    }

}
