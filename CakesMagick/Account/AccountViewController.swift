//
//  AccountViewController.swift
//  CakesMagick
//
//  Created by Ajmera, Prashuk (Cognizant) on 17/10/17.
//  Copyright © 2017 Cakes N Magick. All rights reserved.
//

import UIKit
import CoreData

class AccountViewController: UITableViewController {

    @IBOutlet weak var signInLbl: UILabel!
    
    var results: [NSManagedObject] = []
    var statusStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if accName == "" {
            self.title = "My Account"
        } else {
            self.title = "Welcome " + accName
        }
        
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "SignedStatus")
        request.returnsObjectsAsFaults = false
        do {
            results = try context.fetch(request) as! [NSManagedObject]
            if results.count > 0 {
                for result in results {
                    if let value = (result.value(forKeyPath: "status") as? String) {
                        statusStr = value
                    }
                }
            }
        } catch let error as NSError {
            print(error)
        }
        
        if statusStr == "signin" {
            signInLbl.text = "Sign Out"
        } else {
            signInLbl.text = "Sign In"
            self.title = "Welcome"
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if statusStr == "signin" {
            if (indexPath == [0,0]) {
//                let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersViewController") as? MyOrdersViewController
//                self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
                performSegue(withIdentifier: "toOrders", sender: nil)
            }
            if (indexPath == [0,1]) {
//                let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "MyWalletViewController") as? MyWalletViewController
//                self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
                performSegue(withIdentifier: "toWallet", sender: nil)
            }
            if (indexPath == [0,2]) {
//                let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "MyAddressesViewController") as? MyAddressesViewController
//                self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
                performSegue(withIdentifier: "toAddresses", sender: nil)
            }
            if (indexPath == [1,0]) {
//                let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SettingsTableViewController") as? SettingsTableViewController
//                self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
                performSegue(withIdentifier: "toSettings", sender: nil)
            }
            if (indexPath == [1,1]) {
                let alert = UIAlertController(title: "Sign Out", message: "Are you sure you want to sign out!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { action in
                    switch action.style{
                    case .default:
                        if let index = self.tableView.indexPathForSelectedRow{
                            self.tableView.deselectRow(at: index, animated: true)
                        }
                    case .cancel:
                        print("Cancel")
                    case .destructive:
                        print("Destructive")
                    }
                }))
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                    switch action.style{
                    case .default:
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let context = appDelegate.persistentContainer.viewContext
                        let updateValue = self.results[0]
                        context.delete(updateValue)
                        do {
                            try context.save()
                        } catch let error as NSError {
                            print(error)
                        }
                        updateValue.setValue("signout", forKey: "status")
                        self.statusStr = "signout"
                        do {
                            try context.save()
                        } catch let error as NSError {
                            print(error)
                        }
                        self.signInLbl.text = "Sign In"
                        self.title = "Welcome"
                        if let index = self.tableView.indexPathForSelectedRow{
                            self.tableView.deselectRow(at: index, animated: true)
                        }
                    case .cancel:
                        print("Cancel")
                    case .destructive:
                        print("Destructive")
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
//            let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SignInWindow") as? SignInViewController
//            self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
            performSegue(withIdentifier: "toSignIn", sender: nil)
        }
    }
    
    @IBAction func backToAccountsPage(segue:UIStoryboardSegue) { }

}
