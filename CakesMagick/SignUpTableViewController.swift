//
//  SignUpTableViewController.swift
//  CakesMagick
//
//  Created by Ajmera, Prashuk (Cognizant) on 31/10/17.
//  Copyright © 2017 Cakes N Magick. All rights reserved.
//

import UIKit
import CoreData

var personDetails = [(String, [String])]()

class SignUpTableViewController: UITableViewController {

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var emailId: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var cPassword: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    var flag = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2
        self.imageView.clipsToBounds = true
        self.imageView.layer.borderWidth = 3.0
        self.imageView.layer.borderColor = UIColor.gray.cgColor
        
        firstName.delegate = self
        lastName.delegate = self
        emailId.delegate = self
        phoneNumber.delegate = self
        password.delegate = self
        cPassword.delegate = self
        
//        addToolBar(textField: firstName)
//        addToolBar(textField: lastName)
//        addToolBar(textField: emailId)
//        addToolBar(textField: phoneNumber)
//        addToolBar(textField: password)
//        addToolBar(textField: cPassword)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
        self.imageView.image = uploadImage
        self.imageView.contentMode = .scaleAspectFit
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath == [2,0]) {
            detailsOfPerson()
        }
        if (indexPath == [1,0]) {
            performSegue(withIdentifier: "toPhoto", sender: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            lastName.becomeFirstResponder()
        } else if textField.tag == 2 {
            emailId.becomeFirstResponder()
        } else if textField.tag == 3 {
            phoneNumber.becomeFirstResponder()
        } else if textField.tag == 4 {
            password.becomeFirstResponder()
        } else if textField.tag == 5 {
            cPassword.resignFirstResponder()
        } else if textField.tag == 6 {
            cPassword.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func SignUp_Clicked(_ sender: Any) {
        detailsOfPerson()
    }
    
    func detailsOfPerson() {
        if (firstName.text! != "" && lastName.text! != "" && emailId.text! != "" && phoneNumber.text! != "" && password.text! != "" && cPassword.text! != "") {
            if password.text! == cPassword.text! {
                if !isValidEmailAddress(value: emailId.text!) {
                    let alert = UIAlertController(title: "Email Id not valid", message: "Please use correct email id.\nEg. abc@xyz.com", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { action in
                        switch action.style{
                        case .default:
                            if let index = self.tableView.indexPathForSelectedRow{
                                self.tableView.deselectRow(at: index, animated: true)
                            }
                            self.emailId.becomeFirstResponder()
                        case .cancel:
                            print("Cancel")
                        case .destructive:
                            print("Destructive")
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else if !isValidPhoneNumber(value: phoneNumber.text!) {
                    let alert = UIAlertController(title: "Phone Number not valid", message: "Please use correct phone number.\nEg. 0123456789", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { action in
                        switch action.style{
                        case .default:
                            if let index = self.tableView.indexPathForSelectedRow{
                                self.tableView.deselectRow(at: index, animated: true)
                            }
                            self.phoneNumber.becomeFirstResponder()
                        case .cancel:
                            print("Cancel")
                        case .destructive:
                            print("Destructive")
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    for (email, _) in personDetails {
                        if (email == emailId.text!) {
                            flag = 1
                            break
                        }
                    }
                    if flag == 0 {
                        addPerson(firstName: firstName.text!, lastName: lastName.text!, emailId: emailId.text!, phoneNumber: phoneNumber.text!, password: password.text!, gender: "Not Available", DOB: "Not Available")
                        let alert = UIAlertController(title: "Congratulations!", message: "You've successfully registered.\nPlease sign in again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Sign In", style: UIAlertActionStyle.default, handler: { action in
                            switch action.style{
                            case .default:
                                if let index = self.tableView.indexPathForSelectedRow{
                                    self.tableView.deselectRow(at: index, animated: true)
                                }
                                self.performSegue(withIdentifier: "toSignIn", sender: nil)
                            case .cancel:
                                print("Cancel")
                            case .destructive:
                                print("Destructive")
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else if flag == 1 {
                        let alert = UIAlertController(title: "Already Registered", message: "Please use different email id to register.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { action in
                            switch action.style{
                            case .default:
                                if let index = self.tableView.indexPathForSelectedRow{
                                    self.tableView.deselectRow(at: index, animated: true)
                                }
                                self.emailId.becomeFirstResponder()
                            case .cancel:
                                print("Cancel")
                            case .destructive:
                                print("Destructive")
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                        flag = 0
                    }
                }
            } else {
                let alert = UIAlertController(title: "Error!", message: "Both password didn't match.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { action in
                    switch action.style{
                    case .default:
                        if let index = self.tableView.indexPathForSelectedRow{
                            self.tableView.deselectRow(at: index, animated: true)
                        }
                        self.cPassword.becomeFirstResponder()
                    case .cancel:
                        print("Cancel")
                    case .destructive:
                        print("Destructive")
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Error!", message: "Fields can't be empty", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { action in
                switch action.style{
                case .default:
                    if let index = self.tableView.indexPathForSelectedRow{
                        self.tableView.deselectRow(at: index, animated: true)
                    }
                    if self.firstName.text! == "" {
                        self.firstName.becomeFirstResponder()
                    } else if self.lastName.text! == "" {
                        self.lastName.becomeFirstResponder()
                    } else if self.emailId.text! == "" {
                        self.emailId.becomeFirstResponder()
                    } else if self.phoneNumber.text! == "" {
                        self.phoneNumber.becomeFirstResponder()
                    } else if self.password.text! == "" {
                        self.password.becomeFirstResponder()
                    } else if self.cPassword.text! == "" {
                        self.cPassword.becomeFirstResponder()
                    }
                case .cancel:
                    print("Cancel")
                case .destructive:
                    print("Destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func addPerson(firstName: String, lastName: String, emailId: String, phoneNumber: String, password: String, gender: String, DOB: String) {
        personDetails.append((emailId, [firstName, lastName, emailId, phoneNumber, password, gender, DOB]))
    }
    
    func isValidEmailAddress(value: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: value)
        return result
    }
    
    func isValidPhoneNumber(value: String) -> Bool {
        let phoneRegEx = "^\\d{10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegEx)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
}
