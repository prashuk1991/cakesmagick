//
//  SignInViewController.swift
//  CakesMagick
//
//  Created by Ajmera, Prashuk (Cognizant) on 17/10/17.
//  Copyright © 2017 Cakes N Magick. All rights reserved.
//

import UIKit
import CoreData

var accName = ""

class SignInViewController: UITableViewController {
    
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    var flag = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTxt.delegate = self
        passwordTxt.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath == [1,0]) {
            signInDetails()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            passwordTxt.becomeFirstResponder()
        }
        if textField.tag == 2 {
            passwordTxt.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func signInBtnTap(_ sender: Any) {
        signInDetails()
    }
    
    func signInDetails() {
        if (emailTxt.text == "" || passwordTxt.text == "") {
            let alert = UIAlertController(title: "Error!", message: "Fields can't be empty", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { action in
                switch action.style {
                    case .default:
                        if let index = self.tableView.indexPathForSelectedRow {
                            self.tableView.deselectRow(at: index, animated: true)
                        }
                    case .cancel:
                        print("Cancel")
                    case .destructive:
                        print("Destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            for (email, password) in personDetails {
                if (email == emailTxt.text!) {
                    if (password[4] == passwordTxt.text!) {
                        flag = 0
                        accName = password[0]
                        break
                    } else {
                        flag = 2
                    }
                } else {
                    flag = 1
                }
            }
            if flag == 0 {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let context = appDelegate.persistentContainer.viewContext
                let status = NSEntityDescription.insertNewObject(forEntityName: "SignedStatus", into: context)
                status.setValue("signin", forKey: "status")
                do {
                    try context.save()
                } catch let error as NSError {
                    print(error)
                }
                performSegue(withIdentifier: "backToAccountsPage", sender: self)
            } else if flag == 1 {
                let alert = UIAlertController(title: "Error!", message: "Email id doesn't exist.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { action in
                    switch action.style{
                    case .default:
                        if let index = self.tableView.indexPathForSelectedRow{
                            self.tableView.deselectRow(at: index, animated: true)
                        }
                        self.emailTxt.becomeFirstResponder()
                    case .cancel:
                        print("Cancel")
                    case .destructive:
                        print("Destructive")
                    }
                }))
                flag = 0
                self.present(alert, animated: true, completion: nil)
            } else if flag == 2 {
                let alert = UIAlertController(title: "Error!", message: "Email / Password doesn't match", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler: { action in
                    switch action.style{
                    case .default:
                        if let index = self.tableView.indexPathForSelectedRow{
                            self.tableView.deselectRow(at: index, animated: true)
                        }
                        self.passwordTxt.becomeFirstResponder()
                    case .cancel:
                        print("Cancel")
                    case .destructive:
                        print("Destructive")
                    }
                }))
                flag = 0
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func backToSignInPage(segue:UIStoryboardSegue) {}
}
